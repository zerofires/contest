<?php
/*
 * @Date: 2021-09-20 12:14:54
 * @LastEditTime: 2022-05-07 00:26:07
 * @Description: 前台api接口
 * @FilePath: \contest\action.php
 */
require("./Core/ExaminService.php");
require("./Models/ResultModel.class.php");
require("./Models/AccessModel.class.php");

use models\ResultModel;
use models\AccessModel;
use core\ExaminService;

session_start();

if (isset($_GET["a"])) {
    ExaminService::initialize();
}

$action = $_GET["a"];
switch ($action) {
    case "login": // 账号登陆
        login();
        break;
    case "logout": // 退出登录
        logout();
        break;
    case "commit": // 答题提交
        fraction_calculate();
        break;
    case "":
        break;
    default:
        die("错误请求！");
}

/**
 * 登录
 */
function login()
{
    // 启用 session 功能
    session_start();
    // 请求参数验证
    if (!isset($_POST['name']) || !isset($_POST['pwd'])) {
        ResultModel::Fail("参数校验失败！");
    }

    // 请求参数
    $account = $_POST['name'];
    $password = $_POST['pwd'];
    // 验证账号
    $result = ExaminService::Login($account, $password);
    if ($result != null) {

        // 检查是否参加了考试
        $record = ExaminService::Participated($result['no']);
        $score = $record['score'];
        if (strlen($score) > 0 && is_numeric($score)) {
            ResultModel::Ok("已经参加了考试", $score);
        }  // 显示分数
        else {
            $amode = new AccessModel($result['level'], $result['id'], $result['name'], $result['no']);
            // 保存用户信息
            $_SESSION['access'] = serialize($amode);

            ResultModel::Ok("登录成功！", $result['level']);
        } // 登录成功, 返回用户信息
    } else {
        ResultModel::Fail("学号或密码错误！");  
    }
}

/**
 * @description: 退出登录
 * @param {*} $url
 * @return {*}
 */
function logout($url = 'login.html')
{
    session_unset();
    session_destroy();
    ResultModel::Ok();
}

/**
 * @description: 分数计算
 * @param {*}
 * @return {*}
 */
function fraction_calculate()
{
    $amode = unserialize($_SESSION['access']);
    $sno = $amode->no;
    if (strlen($sno) < 1) {
        ResultModel::Fail("请先登录");
    }

    $answers = $_POST['answers'];
    $count = $_POST['count'];
    // 切割分数
    $id_ans = explode(',', $answers);
    // 正确的数量
    $rightCount = 0;
    $totalCount = $count;


    $sql = 'select * from ques';
    // 查询数据
    $result = ExaminService::GetAllSubjects();
    // 获取一条数据
    for ($i = 0; $i < count($result); $i++) {
        $row = $result[$i];
        // 切割数据获取 id 数组
        $id_ans = explode(',', $answers);
        foreach ($id_ans as $item) {
            // 再次切割 id 获取题目id和答题选项
            $temp = explode('_', $item);
            // 判断是否答题正确
            if ($row['id'] == $temp[0] && $row['answer'] == $temp[1]) {
                $rightCount++;
                break;
            }
        }
    }

    // 分数计算
    $score = round(($rightCount / $totalCount) * 100);
    
    // TODO: 获取 试卷id 和 账号id 一并存入 ques_record 表里
    // 记录数据
    $result = ExaminService::SaveFraction($sno, $answers, $score);
    if ($result) {
        // 返回 json 数据
        ResultModel::Ok("", '{"rightNum":' . $rightCount . ',"score":' . $score . '}');
        // echo '{"status":"ok","rightNum":'.$rightCount.',"score":'.$score.'}';
        return;
    } else {
        echo '保存失败';
    }
}
