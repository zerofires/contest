# contest

#### 介绍

使用php写的一个考试系统

#### 软件架构

涉及到的技术: php、layui、jquery、mysql
使用的框架: layuimini
使用的工具: VSCode、Navicat、phpstudy

#### 安装教程

1. 直接使用  **phpstudy**  导入此项目
2. 数据库文件在 **data** 文件夹里

### 使用教程

1. **index.html** 是登录页面，输入不同类型的账号分别进去学生答题页面和后台页面
2. 数据库交互在 **core** 文件夹里

### 页面展示

![登录页面展示](https://gitee.com/FlameLing/contest/raw/master/display_images/login.jpg)

![教师管理页面](https://gitee.com/FlameLing/contest/raw/master/display_images/mana_teacher.jpg)

![添加教师](https://gitee.com/FlameLing/contest/raw/master/display_images/add_teacher.jpg)

![学生管理页面](https://gitee.com/FlameLing/contest/raw/master/display_images/mana_student.jpg)

![添加学生](https://gitee.com/FlameLing/contest/raw/master/display_images/add_student.jpg)

![分配试卷](https://gitee.com/FlameLing/contest/raw/master/display_images/assign_paper.jpg)

![试卷管理](https://gitee.com/FlameLing/contest/raw/master/display_images/mana_paper.jpg)

![添加试卷](https://gitee.com/FlameLing/contest/raw/master/display_images/add_paper.jpg)

![题目管理](https://gitee.com/FlameLing/contest/raw/master/display_images/mana_question.jpg)

![准备答题](https://gitee.com/FlameLing/contest/raw/master/display_images/pre_examination.jpg)

![提交答题](https://gitee.com/FlameLing/contest/raw/master/display_images/end_examination.jpg)
