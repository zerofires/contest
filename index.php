<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>答题</title>
    <!--网页标题左侧显示-->
    <link rel="icon" href="./static/imgs/favicon.ico" type="image/x-icon">
    <!--收藏夹显示图标-->
    <link rel="shortcut icon" href="./static/imgs/favicon.ico" type="image/x-icon" />
    <link href="static/css/site.css" rel="stylesheet" type="text/css" />
    <script src="static/js/jquery-3.5.1.min.js"></script>
    <script src="static/js/layer.js"></script>
    <script src="static/js/common.js"></script>
    <style>
        .header div {
            display: flex;
            align-items: center;
            margin-right: 10%;
        }

        .finalPop {
            display: flex;
            flex-direction: column;
            padding-top: 50px;
            justify-items: center;
            align-items: center;
        }

        .finalPop p {
            margin: 10px 0;
        }
    </style>
</head>

<body>
    <div class="container">
        <?php
        require_once('./core/ExaminService.php');
        require_once('./models/AccessModel.class.php');

        use core\ExaminService;

        session_start();
        if (!isset($_SESSION['access'])) {
            echo "<script>
                        $(()=>{
                            let loading = layer.msg('你还未登录，1 秒后跳转到登录页！', { icon: 16, shade: 0.3, time: 0 });
                            setTimeout(function () {
                                layer.close(loading);
                                let url = 'login.html';
                                window.location = url;
                            }, 1000);
                        });
                    </script>";
            exit();
        }
        $access = unserialize($_SESSION['access']);
        if ($access->type != '1') {
            echo "<script>
                        $(()=>{
                            let loading = layer.msg('你还未登录，1 秒后跳转到登录页！', { icon: 16, shade: 0.3, time: 0 });
                            setTimeout(function () {
                                layer.close(loading);
                                let url = 'login.html';
                                window.location = url;
                            }, 1000);
                        });
                    </script>";
            exit();
        }

        // 学生id
        $id = $access->id;
        // 获取学生信息
        $userInfo = ExaminService::initialize()::GetPeopleInfo($id);
        // ExaminService::test();

        if ($userInfo == null) {
            header("location: login.html");
            exit();
        }

        ?>

        <div class="header">
            <div id="date_show"></div>
            <div>学号：<?= $userInfo['no'] ?> </div>
            <div>姓名：<?= $userInfo['name'] ?></div>
        </div>
        <div class="content">

            <!-- #region 旧版 -->
            <!-- <?php
                    // $sql = "SELECT * FROM `ques` WHERE type='radio' ORDER BY RAND() LIMIT 20";
                    // $result = mysqli_query($conn, $sql);
                    // 获取题目
                    $result = ExaminService::GetSubjects();
                    // $count = 0;
                    /* while ($row = mysqli_fetch_assoc($result)) { ?> */
                    for ($i = 0; $i < count($result); $i++) {
                        $row = $result[$i];
                    ?>

                <div class="title_header" id="q<?= ++$count ?>">
                    <div class="tag"><?= $count ?>. 单选题</div>
                    <span> </span> <?= $row['question'] ?>
                    <div class="answers">
                        <p data-type="<?= $row['type'] ?>" data-val="<?= $row['id'] ?>_A">A. <?= $row['A'] ?></p> <br />
                        <p data-type="<?= $row['type'] ?>" data-val="<?= $row['id'] ?>_B">B. <?= $row['B'] ?></p><br />
                        <p data-type="<?= $row['type'] ?>" data-val="<?= $row['id'] ?>_C">C. <?= $row['C'] ?></p><br />
                        <p data-type="<?= $row['type'] ?>" data-val="<?= $row['id'] ?>_D">D. <?= $row['D'] ?></p>
                    </div>
                    <div class="answer_tip"></div>
                </div>
                <hr />

            <?php
                    }
            ?> -->
            <!-- #endregion -->

            <!-- #region 新版 -->

            <!-- 1.获取试卷  2.遍历题目 -->

            <?php
            // 获取试卷信息
            $info = ExaminService::GetPaper($id);

            // 获取题目
            $pid = $info['pid'];
            $list = ExaminService::GetQues($pid);

            for ($i = 0; $i < count($list); $i++) {
                $row = $list[$i];
                if (strlen($row['id']) < 1) continue;
            ?>

                <div class="title_header" data-type="<?= $row['t'] ?>" data-id="<?= $row['id'] ?>" data-index="<?= $i + 1 ?>" id="q<?= $i + 1 ?>">
                    <div class="tag"><?= $i + 1 ?>.
                        <?php
                        if ($row['t'] == 'radio') {
                            echo '单选题';
                        } else if ($row['t'] == 'checkbox') {
                            echo '多选题';
                        } else if ($row['t'] == 'judge') {
                            echo '是非题';
                        }
                        ?>
                    </div>
                    <span> </span> <?= $row['q'] ?>
                    <div class="answers">
                        <p data-val="A">A. <?= $row['A'] ?></p> <br />
                        <p data-val="B">B. <?= $row['B'] ?></p><br />
                        <?php
                        if ($row['t'] != 'judge') {
                        ?>
                            <p data-val="C">C. <?= $row['C'] ?></p><br />
                            <p data-val="D">D. <?= $row['D'] ?></p>
                        <?php
                        }
                        ?>
                    </div>
                    <div class="answer_tip"></div>
                </div>
                <hr />

            <?php
            }
            ?>

            <!-- #endregion -->

        </div>
        <div class="info">
            <span id="retainTime" data-time='<?= $info['minutes'] ?>'>剩余时间：<?= $info['minutes'] ?>分钟</span>
            <div class="progress">
                进度：
                <div class="point_container">
                    <script>
                        let count = $('.title_header').length;
                        let obj = $('.point_container');
                        for (let i = 1; i <= count; i++) {
                            obj.append(`<a class="p_point" href="#q${i}"></a>`);
                        }
                    </script>
                </div>
            </div>


            <div class="rightDiv">
                <div class="btn cancel" id="cancel">取消答题</div>
                <div class="btn" id="ok">结束答题</div>
            </div>

        </div>
    </div>
    <script src="static/js/index.js"></script>
</body>

</html>