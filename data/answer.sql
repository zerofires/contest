/*
 Navicat Premium Data Transfer

 Source Server         : answer
 Source Server Type    : MySQL
 Source Server Version : 80012
 Source Host           : localhost:3306
 Source Schema         : answer

 Target Server Type    : MySQL
 Target Server Version : 80012
 File Encoding         : 65001

 Date: 13/01/2022 17:25:39
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for people_account
-- ----------------------------
DROP TABLE IF EXISTS `people_account`;
CREATE TABLE `people_account`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `no` varchar(15) CHARACTER SET gbk COLLATE gbk_chinese_ci NOT NULL COMMENT '学号 或 老师工号 或 管理员操作号',
  `name` varchar(11) CHARACTER SET gbk COLLATE gbk_chinese_ci NOT NULL,
  `password` varchar(11) CHARACTER SET gbk COLLATE gbk_chinese_ci NOT NULL,
  `gender` varchar(1) CHARACTER SET gbk COLLATE gbk_chinese_ci NOT NULL DEFAULT '女',
  `level` int(1) NOT NULL DEFAULT 0 COMMENT '1 学生 2 老师 3 管理员',
  `phone` varchar(30) CHARACTER SET gbk COLLATE gbk_chinese_ci NULL DEFAULT NULL COMMENT '手机号',
  `email` varchar(255) CHARACTER SET gbk COLLATE gbk_chinese_ci NULL DEFAULT NULL COMMENT '邮箱',
  `remarks` varchar(255) CHARACTER SET gbk COLLATE gbk_chinese_ci NULL DEFAULT NULL COMMENT '备注',
  `pid` int(11) NULL DEFAULT NULL COMMENT '考试的试卷id',
  `createDate` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `index`(`no`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 19 CHARACTER SET = gbk COLLATE = gbk_chinese_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of people_account
-- ----------------------------
INSERT INTO `people_account` VALUES (1, '20180101', 'xiao', '123456', '女', 1, '14523238989', '124245@qq.com', '谁让你心动谁让你心痛', 1, '2021-09-20 10:51:20');
INSERT INTO `people_account` VALUES (2, '00', 'admin', '123456', '女', 3, NULL, NULL, NULL, NULL, '2021-09-20 10:53:12');
INSERT INTO `people_account` VALUES (3, '20180102', 'fre', '123456', '男', 1, '17845236445', 'fire123@qq.com', '东很懂根底跟东哥', 8, '2021-09-20 10:51:20');
INSERT INTO `people_account` VALUES (5, '116', '张三', '123456', '男', 2, '15180452323', 'fire123@qq.com', 'hello world', NULL, '2021-10-07 20:46:48');
INSERT INTO `people_account` VALUES (6, '121', '王五', '123456', '男', 2, '15180452323', '', '', NULL, '2021-10-07 20:50:19');
INSERT INTO `people_account` VALUES (10, '20180103', '小白', '123456', '女', 1, '15423235656', '', '跟东很懂', 1, '2021-10-19 21:14:25');
INSERT INTO `people_account` VALUES (9, '111', '李四', '123456', '男', 2, '16180452323', '', '', NULL, '2021-10-19 20:58:50');
INSERT INTO `people_account` VALUES (18, '201245', '小花', '123456', '女', 1, '', '', '', 1, '2021-11-28 17:09:43');

-- ----------------------------
-- Table structure for ques
-- ----------------------------
DROP TABLE IF EXISTS `ques`;
CREATE TABLE `ques`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accountId` int(11) NULL DEFAULT NULL COMMENT '导入题目的用户id',
  `question` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `answer` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `A` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `B` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `C` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `D` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `createDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 61 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ques
-- ----------------------------
INSERT INTO `ques` VALUES (1, NULL, 'PHP哪个函数能取得字符串长度?', 'C', 'radio', 'strrev', 'substr', 'strlen', 'strchr', '2021-10-08 20:08:55');
INSERT INTO `ques` VALUES (2, NULL, '关于PHP中的各种循环,说法正确的是:', 'A', 'radio', 'foreach 语句用于循环遍历数组', 'do...while 是先判断再运行循环', 'while 是先循环再判断条件', 'for循环是条件判断型的循环,跟while相似', '2021-10-08 20:08:55');
INSERT INTO `ques` VALUES (3, NULL, '下列哪些是PHP的配置文件?', 'B', 'radio', 'php.exe', 'php.ini', 'php_mysql.dll', 'php_mysqli.dll', '2021-10-08 20:08:55');
INSERT INTO `ques` VALUES (4, NULL, 'PHP中整数型数据类型,在32位操作系统中表示的范围是多少?', 'D', 'radio', '-256到256', '-1024到1024', '-32767到32767', '-2147483648到2147483647', '2021-10-08 20:08:55');
INSERT INTO `ques` VALUES (5, NULL, 'PHP中应该如何声明变量?', 'A', 'radio', '采用$号开头后面跟变量名', '采用var开头后面跟变量名', '采用declare开头后面跟变量名', '直接写出变量名就可以', '2021-10-08 20:08:55');
INSERT INTO `ques` VALUES (6, NULL, 'PHP字符串连接运算符是:', 'B', 'radio', '+', '.', '&&', '!', '2021-10-08 20:08:55');
INSERT INTO `ques` VALUES (7, NULL, '哪些是正确的 逻辑或运算符号?', 'A', 'radio', 'or', '&&', ' !', '||', '2021-10-08 20:08:55');
INSERT INTO `ques` VALUES (8, NULL, '哪些是MySQL自带的数据库?', 'A', 'radio', 'information_schema', 'sanguo', 'tempdb', 'master', '2021-10-08 20:08:55');
INSERT INTO `ques` VALUES (9, NULL, '? : 运算符相当于以下哪个PHP 语句?', 'A', 'radio', 'if...else', 'switch', 'for', 'break', '2021-10-08 20:08:55');
INSERT INTO `ques` VALUES (10, NULL, '以下哪些是PHP的会话控制技术?', 'B', 'radio', 'Cookie', 'Session', 'Application', 'Server', '2021-10-08 20:08:55');
INSERT INTO `ques` VALUES (11, NULL, '以下哪些是单词的说法是正确的?', 'D', 'radio', 'insert ---用来修改数据', 'update ---用来删除数据', 'delete ---用来添加数据', 'select ---用来查看数据', '2021-10-08 20:08:55');
INSERT INTO `ques` VALUES (12, NULL, '以下哪个mysql命令可以查看数据表的结构信息?', 'B', 'radio', 'show tables', 'describe 表名', 'create table 表名', 'select * from 表名?', '2021-10-08 20:08:55');
INSERT INTO `ques` VALUES (13, NULL, '以下哪个SQL语句是正确的', 'D', 'radio', 'insert into users values(‘p001’,’张三’,’男’);', 'create table 表名(Code int primary key);', 'update users set Code=’p002’ where Code=’p001’;', 'select Code as ‘代号’ from users;', '2021-10-08 20:08:55');
INSERT INTO `ques` VALUES (14, NULL, 'php定义变量正确的是:', 'B', 'radio', 'var a = 5;', '$a = 10;', 'int b = 6;', 'var $a = 12;', '2021-10-08 20:08:55');
INSERT INTO `ques` VALUES (15, NULL, 'php中单引号和双引号包含字符串的区别正确的是:', 'D', 'radio', '单引号速度快，双引号速度慢', '双引号速度快，单引号速度慢', '单引号里面可以解析转义字符', '双引号里面可以解析变量', '2021-10-08 20:08:55');
INSERT INTO `ques` VALUES (16, NULL, 'php中关于字符串处理函数以下说法正确的是:', 'C', 'radio', 'implode( )方法可以将字符串拆解为数组合并', 'str_replace()可以替换指定位置的字符串查找替换', 'substr( )可以截取字符串', 'strlen( )不能取到字符串的长度', '2021-10-08 20:08:55');
INSERT INTO `ques` VALUES (17, NULL, '下列说法正确的是：', 'B', 'radio', '数组的下标必须为数字，且从“0”开始', '数组的下标可以是字符串 弱类型语言', '数组中的元素类型必顺一致', '数组的下标必须是连续的', '2021-10-08 20:08:55');
INSERT INTO `ques` VALUES (18, NULL, '下面哪项描述是错误的?', 'C', 'radio', '普通成员是属于对象的', '成员变量需要用public protected private修饰，在定义变量时不再需要var关键字', '静态成员是属于对象的', '包含抽象方法的类必须为抽象类，抽象类不能被实例化', '2021-10-08 20:08:55');
INSERT INTO `ques` VALUES (19, NULL, '关于exit( )与die( )的说法正确的是:', 'C', 'radio', '当exit( )函数执行会停止执行下面的脚本，而die()无法做到', '当die()函数执行会停止执行下面的脚本，而exit( )无法做到', '使用die()函数的地方也可以使用exit()函数替换', 'die()函数和exit()函数没有区别', '2021-10-08 20:08:55');
INSERT INTO `ques` VALUES (20, NULL, 'php输出拼接字符串正确的是:', 'C', 'radio', 'echo $a+”hello”', 'echo a+b', 'echo $a.”hello”', 'echo ‘{$a}hello’', '2021-10-08 20:08:55');
INSERT INTO `ques` VALUES (21, NULL, '在用浏览器查看网页时出现404错误可能的原因是:', 'B', 'radio', '页面源代码错误 500', '文件不存在', '与数据库连接错误', '权限不足', '2021-10-08 20:08:55');
INSERT INTO `ques` VALUES (22, NULL, '面向对象的三大特性中哪个不属于封装的做法?', 'B', 'radio', '将成员变为私有的', '将成员变为公有的', '封装方法来操作成员', '使用__get()和__set()方法来操作成员', '2021-10-08 20:08:55');
INSERT INTO `ques` VALUES (23, NULL, '下面哪个选项没有将 john 添加到 users 数组中?', 'B', 'radio', '$users[] = “john”;', 'array_add($users, “john”);', 'array_push($users, “john”);', '$users [“aa”]= “john” ;', '2021-10-08 20:08:55');
INSERT INTO `ques` VALUES (24, NULL, 'php中以下能输出1到10之间的随机数的是:', 'C', 'radio', 'echo rand();', 'echo rand()*10;', 'echo rand(1,10);', 'echo rand(10);', '2021-10-08 20:08:55');
INSERT INTO `ques` VALUES (25, NULL, 'php中以下能输出当前时间格式像：2021-5-20 5:20:13 的是:', 'A', 'radio', 'echo date(“Y-m-d H:i:s”);', 'echo time();', 'echo date();', 'echo time(“Y-m-d H:i:s”);', '2021-10-08 20:08:55');
INSERT INTO `ques` VALUES (26, NULL, 'php中，不等运算符是：', 'BC', 'checkbox', '≠', '!=', '<>', '><', '2021-10-08 20:08:55');
INSERT INTO `ques` VALUES (27, NULL, '函数的参数传递包括：', 'AB', 'checkbox', '按值传递', '按引用传递', '按变量传递', '按作用域传递', '2021-10-08 20:08:55');
INSERT INTO `ques` VALUES (28, NULL, 'php中，赋值运算符有：', 'ABD', 'checkbox', '=', '+=', '==', '.=', '2021-10-08 20:08:55');
INSERT INTO `ques` VALUES (29, NULL, 'php中可以实现程序分支结构的关键字是：', 'CD', 'checkbox', 'while', 'for', 'if', 'swhich', '2021-10-08 20:08:55');
INSERT INTO `ques` VALUES (30, NULL, 'continue语句可以用在( )中。', 'ABC', 'checkbox', 'for', 'while', 'do-while', 'switch', '2021-10-08 20:08:55');
INSERT INTO `ques` VALUES (31, NULL, 'break可以用在( )语句中。', 'ABCD', 'checkbox', 'switch', 'for', 'while', 'do-while', '2021-10-08 20:08:55');
INSERT INTO `ques` VALUES (33, NULL, 'php中可以实现循环的是：', 'AC', 'checkbox', 'for', 'break', 'while', 'waiting', '2021-10-08 20:08:55');
INSERT INTO `ques` VALUES (34, NULL, 'PHP中,标识符允许出现的符号有:', 'ABC', 'checkbox', '大写字母', '小写字母', '数字', '减号', '2021-10-08 20:08:55');
INSERT INTO `ques` VALUES (35, NULL, 'PHP允许的注释符号有:', 'ACD', 'checkbox', '//', '闭合的段落', '#', '/*和*/闭合的段落', '2021-10-08 20:08:55');
INSERT INTO `ques` VALUES (36, NULL, 'PHP表单的提交方法有:', 'ABC', 'checkbox', 'post', 'request', 'get', 'querystring', '2021-10-08 20:08:55');
INSERT INTO `ques` VALUES (37, NULL, 'php中布尔类型数据只有两个值：真和假。', 'A', 'judge', '对', '错', '', '', '2021-10-08 20:08:55');
INSERT INTO `ques` VALUES (38, NULL, 'php中连接两个字符串的符号是“+ ”。', 'B', 'judge', '对', '错', '', '', '2021-10-08 20:08:55');
INSERT INTO `ques` VALUES (39, NULL, 'php可以使用“scanf”来打印输出结果。', 'B', 'judge', '对', '错', '', '', '2021-10-08 20:08:55');
INSERT INTO `ques` VALUES (40, NULL, '每条语句结尾都要加“；”来表示语句结束。', 'A', 'judge', '对', '错', '', '', '2021-10-08 20:08:55');
INSERT INTO `ques` VALUES (41, NULL, 'php变量使用之前需要定义变量类型。', 'B', 'judge', '对', '错', '', '', '2021-10-08 20:08:55');
INSERT INTO `ques` VALUES (42, NULL, '在php中“==”的意思是“等于”。', 'A', 'judge', '对', '错', '', '', '2021-10-08 20:08:55');
INSERT INTO `ques` VALUES (43, NULL, 'while和do-while语句都是先判断条件再执行循环体。', 'B', 'judge', '对', '错', '', '', '2021-10-08 20:08:55');
INSERT INTO `ques` VALUES (44, NULL, '“break ”代表的意思是跳出循环。', 'A', 'judge', '对', '错', '', '', '2021-10-08 20:08:55');
INSERT INTO `ques` VALUES (45, NULL, '若定义数组时省略关键字key，则第三个数组元素的关键字为3。', 'B', 'judge', '对', '错', '', '', '2021-10-08 20:08:55');
INSERT INTO `ques` VALUES (46, NULL, 'mysql数据库中查询数据用select语句。', 'A', 'judge', '对', '错', '', '', '2021-10-08 20:08:55');
INSERT INTO `ques` VALUES (55, 1, '第一个', 'a', 'judge', '234', '5353', '', '', '2022-01-11 22:00:35');
INSERT INTO `ques` VALUES (60, 1, '第一个dfgfdgfdsg', 'a', 'judge', '532', '5353', '', '', '2022-01-11 22:23:54');

-- ----------------------------
-- Table structure for ques_pape_builder
-- ----------------------------
DROP TABLE IF EXISTS `ques_pape_builder`;
CREATE TABLE `ques_pape_builder`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `quesId` int(11) NOT NULL COMMENT '问题id',
  `paperId` int(11) NOT NULL COMMENT '试卷id',
  `fraction` float NULL DEFAULT NULL COMMENT '此题分数',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 44 CHARACTER SET = gbk COLLATE = gbk_chinese_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of ques_pape_builder
-- ----------------------------
INSERT INTO `ques_pape_builder` VALUES (1, 1, 1, NULL);
INSERT INTO `ques_pape_builder` VALUES (2, 2, 1, NULL);
INSERT INTO `ques_pape_builder` VALUES (3, 3, 1, NULL);
INSERT INTO `ques_pape_builder` VALUES (4, 4, 1, NULL);
INSERT INTO `ques_pape_builder` VALUES (5, 5, 1, NULL);
INSERT INTO `ques_pape_builder` VALUES (6, 6, 1, NULL);
INSERT INTO `ques_pape_builder` VALUES (7, 6, 2, NULL);
INSERT INTO `ques_pape_builder` VALUES (8, 8, 2, NULL);
INSERT INTO `ques_pape_builder` VALUES (34, 14, 2, NULL);
INSERT INTO `ques_pape_builder` VALUES (43, 23, 1, NULL);
INSERT INTO `ques_pape_builder` VALUES (42, 15, 1, NULL);
INSERT INTO `ques_pape_builder` VALUES (41, 7, 1, NULL);
INSERT INTO `ques_pape_builder` VALUES (40, 39, 1, NULL);
INSERT INTO `ques_pape_builder` VALUES (39, 30, 1, NULL);
INSERT INTO `ques_pape_builder` VALUES (38, 22, 1, NULL);
INSERT INTO `ques_pape_builder` VALUES (37, 14, 1, NULL);
INSERT INTO `ques_pape_builder` VALUES (36, 55, 1, NULL);
INSERT INTO `ques_pape_builder` VALUES (35, 60, 1, NULL);
INSERT INTO `ques_pape_builder` VALUES (22, 60, 8, NULL);
INSERT INTO `ques_pape_builder` VALUES (25, 55, 8, NULL);
INSERT INTO `ques_pape_builder` VALUES (32, 60, 2, NULL);
INSERT INTO `ques_pape_builder` VALUES (29, 14, 8, NULL);
INSERT INTO `ques_pape_builder` VALUES (30, 22, 8, NULL);
INSERT INTO `ques_pape_builder` VALUES (33, 55, 2, NULL);

-- ----------------------------
-- Table structure for ques_record
-- ----------------------------
DROP TABLE IF EXISTS `ques_record`;
CREATE TABLE `ques_record`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accountId` int(11) NULL DEFAULT NULL,
  `studentNo` varchar(15) CHARACTER SET gbk COLLATE gbk_chinese_ci NOT NULL,
  `paperId` int(11) NULL DEFAULT NULL COMMENT '试卷id',
  `record` varchar(1024) CHARACTER SET gbk COLLATE gbk_chinese_ci NOT NULL,
  `score` float NOT NULL DEFAULT 0,
  `createDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 40 CHARACTER SET = gbk COLLATE = gbk_chinese_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ques_record
-- ----------------------------
INSERT INTO `ques_record` VALUES (38, 1, '20180101', NULL, '21_B,16_B,23_B,24_C,17_C,4_C,14_A,25_C,20_C,5_C,19_A,22_C,10_C,12_C,3_C,18_C,11_B,15_B,13_A,8_B', 25, '2021-06-16 01:56:47');
INSERT INTO `ques_record` VALUES (40, NULL, '', NULL, '1_C,2_,3_,4_,5_,6_,23_,15_,7_,39_,30_,22_,14_,55_,60_', 7, '2022-01-12 21:55:07');
INSERT INTO `ques_record` VALUES (41, NULL, '', NULL, '1_C,2_,3_,4_,5_,6_,23_,15_,7_,39_,30_,22_,14_,55_,60_', 7, '2022-01-12 21:57:31');
INSERT INTO `ques_record` VALUES (42, NULL, '1', NULL, '1_C,2_,3_,4_,5_,6_,23_,15_,7_,39_,30_,22_,14_,55_,60_', 7, '2022-01-12 21:59:27');
INSERT INTO `ques_record` VALUES (43, NULL, '20180101', NULL, '1_C,2_,3_,4_,5_,6_,23_,15_,7_,39_,30_,22_,14_,55_,60_', 7, '2022-01-12 22:13:06');
INSERT INTO `ques_record` VALUES (44, NULL, '20180101', NULL, '1_C,2_,3_,4_,5_,6_,23_,15_,7_,39_,30_,22_,14_,55_,60_', 7, '2022-01-12 22:16:48');
INSERT INTO `ques_record` VALUES (45, NULL, '20180101', NULL, '1_C,2_,3_,4_,5_,6_,23_,15_,7_,39_,30_,22_,14_,55_,60_', 7, '2022-01-12 22:19:53');
INSERT INTO `ques_record` VALUES (46, NULL, '20180101', NULL, '1_C,2_,3_,4_,5_,6_,23_,15_,7_,39_,30_,22_,14_,55_,60_', 7, '2022-01-12 22:21:35');
INSERT INTO `ques_record` VALUES (47, NULL, '20180101', NULL, '1_C,2_,3_,4_,5_,6_,23_,15_,7_,39_,30_,22_,14_,55_,60_', 7, '2022-01-12 22:22:33');
INSERT INTO `ques_record` VALUES (48, NULL, '116', NULL, '1_,2_,3_,4_,5_,6_,23_,15_,7_,39_,30_,22_,14_,55_,60_', 0, '2022-01-13 12:39:19');

-- ----------------------------
-- Table structure for test_paper
-- ----------------------------
DROP TABLE IF EXISTS `test_paper`;
CREATE TABLE `test_paper`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) CHARACTER SET gbk COLLATE gbk_chinese_ci NULL DEFAULT NULL COMMENT '试卷标题',
  `accountId` int(11) NULL DEFAULT NULL COMMENT '试卷创建人Id',
  `minutes` int(11) NULL DEFAULT NULL COMMENT '考试时间，单位 分钟',
  `fraction` float(10, 2) NULL DEFAULT NULL COMMENT '总分数',
  `createDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 9 CHARACTER SET = gbk COLLATE = gbk_chinese_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of test_paper
-- ----------------------------
INSERT INTO `test_paper` VALUES (1, 'one test', 1, 30, 100.00, '2021-11-29 17:16:18');
INSERT INTO `test_paper` VALUES (2, '试卷2', 1, 35, 100.00, '2021-11-29 17:16:29');
INSERT INTO `test_paper` VALUES (8, '第一个dfgfdgfdsg', 1, 60, 100.00, '2022-01-12 11:23:28');

SET FOREIGN_KEY_CHECKS = 1;
