<?php

namespace core;

use mysqli;

/**
 * Created by JetBrains PhpStorm.
 * User: lee
 * Date: 13-7-26
 * Time: 下午8:30
 * To change this template use File | Settings | File Templates.
 */
class SqlHelper
{
    private $mysqli;
    private static $host = "localhost";
    private static $user = "root";
    private static $pwd = "root";
    private static $db = "answer";
    private $sql = false;
    private $result = false;

    function __construct()
    {
        $this->mysqli = new \MySQLi(self::$host, self::$user, self::$pwd, self::$db);
        if ($this->mysqli->connect_error) {
            die("连接数据库失败！ " . $this->mysql->connect_error);
        }
        $this->mysqli->query("set names utf8");
    }

    function get_mysqli()
    {
        return $this->mysqli;
    }

    function execute_dql_all($sql)
    {
        //执行查询语句
        $arr = array();
        $this->result = $this->mysqli->query($sql) or die($this->mysql->connect_error);
        //将数据转存到$arr数组中
        while ($row = mysqli_fetch_array($this->result, MYSQLI_BOTH)) {
            $arr[] = $row;
        }
        $this->result->free();
        return $arr;
    }

    function execute_dql_num($sql)
    {
        //执行查询语句
        $arr = array();
        $this->result = $this->mysqli->query($sql) or die($this->mysql->connect_error);
        //将数据转存到$arr数组中
        while ($row = mysqli_fetch_array($this->result, MYSQLI_NUM)) {
            $arr[] = $row;
        }
        // $this->result->free();
        return $arr;
    }

    function execute_dql_assoc($sql)
    {
        //执行查询语句
        $arr = array();
        $this->result = $this->mysqli->query($sql) or die($this->mysql->connect_error);
        //将数据转存到$arr数组中
        while ($row = mysqli_fetch_array($this->result, MYSQLI_ASSOC)) {
            $arr[] = $row;
        }
        // $this->result->free();
        return $arr;
    }

    /**
     * 查询某表中的记录数
     */
    function execute_dql_counts($table, $id = "*", $condition = null)
    {
        $this->sql = "select count($id) from $table a";
        if ($condition != null && trim($condition) !== '') {
            $this->sql .= " where 1 $condition ";
        }
        $this->result = $this->mysqli->query($this->sql);
        $row = mysqli_fetch_all($this->result);
        // $this->result->free();
        return $row[0][0];
    }

    /**
     * @description: 执行增删改
     * @param {*} $sql
     * @return {*}
     */
    function execute_dml($sql)
    {
        //执行正删改
        $this->result = $this->mysqli->query($sql);
        if (!$this->result) {
            return -1; //执行正删改失败
        } else {
            $id = $this->mysqli->insert_id;
            return $id;
        }
    }

    function test($sql)
    {
        $row = $this->mysqli->query($sql);
    }
}
