<?php
/*
 *           佛曰:  
 *                   写字楼里写字间，写字间里程序员；  
 *                   程序人员写程序，又拿程序换酒钱。  
 *                   酒醒只在网上坐，酒醉还来网下眠；  
 *                   酒醉酒醒日复日，网上网下年复年。  
 *                   但愿老死电脑间，不愿鞠躬老板前；  
 *                   奔驰宝马贵者趣，公交自行程序员。  
 *                   别人笑我忒疯癫，我笑自己命太贱；  
 *                   不见满街漂亮妹，哪个归得程序员？
 */


namespace core;

require_once("SqlHelper.php");

ExaminService::$db = new SqlHelper();

class ExaminService
{
  public static $db;


  function __construct()
  {
  }

  /**
   * 初始化静态类
   *
   * @return ExaminService
   */
  public static function initialize(): ExaminService
  {
    if (!self::$db instanceof self) {
      return new self();
    }

    return self::$db;
  }

  /**
   * @description: 登录
   * @param {string} $name
   * @param {string} $pwd
   * @param {string} $type
   * @return {mysqli_result}
   */
  public static function Login($name, $pwd)
  {
    if (is_null($name)) {
      echo "账号不能为空";
      return;
    }
    if (is_null($pwd)) {
      echo "密码不能为空";
      return;
    }

    include_once('conn.php');

    $sql = "SELECT * FROM `people_account` WHERE 1 and (name='$name' or no='$name') and password = '$pwd' ";

    $result = static::$db->execute_dql_assoc($sql);
    if (count($result) > 0) {
      return $result[0];
    }
    return $result;
  }

  /**
   * @description: 获取用户信息
   * @param {*} $id
   * @return {*}
   */
  public static function GetPeopleInfo($id)
  {
    if (is_null($id)) {
      header("location: login.html");
      exit();
    }

    $sql = "select * from people_account where id='$id'";
    $result = static::$db->execute_dql_assoc($sql);
    if ($result) return $result[0];
  }

  /**
   * @description: 获取试题,旧版
   * @param {*} $amount
   * @return {*}
   */
  static function GetSubjects($amount = 20)
  {
    $sql = "SELECT * FROM `ques` WHERE type='radio' ORDER BY RAND() LIMIT " . $amount;
    $result = static::$db->execute_dql_assoc($sql);
    return $result;
  }

  static function Participated($no)
  {
    $sql = "SELECT qr.* FROM ques_record qr where qr.studentNo='$no' order by qr.createDate desc LIMIT 1";
    $result = static::$db->execute_dql_assoc($sql);
    if (count($result) > 0)
      return $result[0];
    return $result;
  }

  /**
   * @description: 获取试题
   * @param {*} $amount
   * @return {*}
   */
  static function GetQues($pid)
  {
    $sql = "SELECT q.id,qp.fraction,q.question as q,q.answer as ans,q.type as t,q.A,q.B,q.C,q.D from ques_pape_builder qp left join ques q on qp.quesId=q.id where qp.paperId=" . $pid;
    $result = static::$db->execute_dql_assoc($sql);
    return $result;
  }

  /**
   * @description: 从试卷里删除题目
   * @param {*} $pid
   * @return {*}
   */
  static function DelQuesFromPaper($pid)
  {
    $sql = "DELETE FROM ques_pape_builder where paperId=" . $pid;
    $result = static::$db->execute_dml($sql);
    return $result;
  }

  /**
   * @description: 获取试卷信息
   * @param {*} $id
   * @return {*}
   */
  static function GetPaper($id)
  {
    $sql = "SELECT t.id pid,t.title ,t.minutes,t.fraction FROM people_account a left join test_paper t on a.pid=t.id where a.id='" . $id . "'";
    $result = static::$db->execute_dql_assoc($sql);
    if (count($result) > 0)
      return $result[0];
    return $result;
  }

  /**
   * @description: 删除试卷信息
   * @param {*} $pid
   * @return {*}
   */
  static function DelPaper($pid)
  {
    $sql = "DELETE FROM test_paper where id=" . $pid;
    $result = static::$db->execute_dml($sql);
    return $result;
  }

  /**
   * @description: 保存分数
   * @param {*} $no
   * @param {*} $answers
   * @param {*} $score
   * @return {*}
   */
  static function SaveFraction($no, $answers, $score)
  {
    $sql = "INSERT INTO `ques_record` (`id`, `studentNo`, `record`, `score`) VALUES (NULL, '$no', '$answers', '$score')";
    $result = static::$db->execute_dml($sql);
    return $result;
  }

  /**
   * @description: 获取所有试题
   * @param {*}
   * @return {*}
   */
  static function GetAllSubjects()
  {
    $sql = 'select * from ques';
    $result = static::$db->execute_dql_assoc($sql);
    return $result;
  }

  /**
   * @description: 获取试卷
   * @param {*} $pid 试卷id
   * @return {*}
   */
  static function GetTestPaper($aid)
  {
    // 获取学生信息
    $sql = "select * from people_accunt where id=$aid";
    $account = static::$db->execute_dql_assoc($sql);
    // 获取试卷id
    $pid = $account[0]['pid'];

    // 获取试卷题目
    $sql = "select a.id as pid,c.id as qid,a.accountId as aid,a.minutes as mins,c.question as q,c.answer as ans,c.type as type,c.A as a,c.B as b,c.C as c,c.D as d from test_paper a left join ques_pape_builder b on a.id=b.paperId left join ques c on b.quesId=c.id where a.id=$pid";
    $result = static::$db->execute_dql_assoc($sql);
    return $result;
  }

  public static function test()
  {
    $sql = "select * from people_account ";
    static::$db->test($sql);
  }
}
