<?php

namespace core;

/**
 * Created by JetBrains PhpStorm.
 * User: lee
 * Date: 13-7-27
 * Time: 下午2:48
 * To change this template use File | Settings | File Templates.
 */
header("Content-type:text/html;charset=utf-8;");
require_once("SqlHelper.php");
class Paging
{
    private $sqlHelper = false;
    private $pageCount = false; //页数
    private $counts = false; //总记录数
    private $returnArr = false; //分页超链接的分页
    function __construct()
    {
        $this->sqlHelper = new SqlHelper();
        $this->returnArr = array();
    }
    /*
   * 参数说明
   *
   * $table 分页时对那个表的数据分页
   * $id 辅助查询当前分页的数据表的总记录数
   * $pageSize 每页显示多少条信息记录数
   * $pagingSize 分页栏每次循环显示出来的个数
   * $nowPage 当前是第几页，默认第一页
   * $href 分页栏的超链接将要往哪里连接
   */
    function paging_prev_next($table, $id = "*", $pageSize, $pagingSize, $nowPage = 1, $href)
    {
        $this->counts = $this->sqlHelper->execute_dql_counts($table, $id);
        $this->pageCount = ceil($this->counts / $pageSize);
        $this->returnArr["count"] = $this->counts;
        $this->returnArr["start"] = ($nowPage - 1) * $pageSize;
        $this->returnArr["limit"] = $pageSize;
        if ($nowPage > $this->pageCount || $nowPage <= 0) {
            return false;
        }
        $t = (ceil($nowPage / $pagingSize) - 1) * $pagingSize + 1;
        $pre = $nowPage - $pagingSize;
        $nex = $nowPage + $pagingSize;
        echo "
      <span class='paging-list-a paging-list-a-withBg'>{$nowPage}/{$this->pageCount}</span>
      <a href='{$href}?nowPage={$pre}' class='paging-list-a'>&lt;</a>";
        for ($i = $t; $i < $t + $pagingSize; $i++) {
            if ($i * $pageSize > $this->pageCount * $pageSize) {
                break;
            } else {
                if ($nowPage == $i) {
                    echo "
          <a href='{$href}?nowPage={$i}' class='paging-list-a paging-list-a-withBg'>{$i}</a>";
                } else {
                    echo "
          <a href='{$href}?nowPage={$i}' class='paging-list-a'>{$i}</a>";
                }
            }
        }
        echo "
      <a href='{$href}?nowPage={$nex}' class='paging-list-a'>&gt;</a>";
        return $this->returnArr;
    }
}
