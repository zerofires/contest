<?php

namespace models;

class ques_pape_builder
{
    public function getName()
    {
        return __CLASS__;
    }

    public $id;
    public $quesId;
    public $paperId;
    public $fraction;
}
