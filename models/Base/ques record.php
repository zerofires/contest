<?php

namespace models;

class ques_record
{
    public function getName()
    {
        return __CLASS__;
    }

    public $id;
    public $studentNo;
    public $record;
    public $score;
    public $createDate;
}
