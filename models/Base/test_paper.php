<?php

namespace models;

class test_paper
{
    public function getName()
    {
        return __CLASS__;
    }

    public $id;
    public $title;
    public $accountId;
    public $minutes;
    public $fraction;
}
