<?php

namespace models;

class people_account
{
    public function getName()
    {
        return __CLASS__;
    }

    public $id;
    public $no;
    public $name;
    public $password;
    public $gender;
    public $level;
    public $email;
    public $createDate;
}
