<?php
namespace models;

class AccessModel {
    /**
     * 1 学生 2 老师 3 管理员
     */
    public $type;
    /**
     * 唯一主键
     */
    public $id;
    /**
     * 名称
     */
    public $name;

    public function __construct($type=null, $id=null, $name=null,$no=null){
        $this->type = $type;
        $this->id = $id;
        $this->name = $name;
        $this->no = $no;
    }
}
?>