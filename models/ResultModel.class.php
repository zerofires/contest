<?php
/*
 * @Author: your name
 * @Date: 2021-09-20 09:00:55
 * @LastEditTime: 2021-10-07 15:32:57
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \contest\models\ResultModel.class.php
 */

namespace models;

class ResultModel
{
    public $success;
    public $message;
    public $data;

    public function __construct($success = true, $message = null, $data = null)
    {
        $this->success = $success;
        $this->message = $message;
        $this->data = $data;
    }

    public function getJson($success = true, $message = null, $data = null)
    {
        $this->success = $success;
        $this->message = $message;
        $this->data = $data;
        ResultModel::getJsonResult($this->success, $this->message);
    }

    public static function getJsonResult($success = true, $message = null, $data = null)
    {
        header("Content-Type: application/json; charset=UTF-8");
        echo json_encode(new ResultModel($success, $message, $data));
        exit();
    }

    public static function Ok($message = null,$data=null)
    {
        self::getJsonResult(true, $message,$data);
    }

    public static function Fail($message = null)
    {
        self::getJsonResult(false, $message);
    }

    /**
     * @description: 没有权限
     * @param {*} $message
     * @return {*}
     */
    public static function NoAuthority($message = null)
    {
        if(is_null($message)) $message = "没有权限";
        self::getJsonResult(false, $message);
    }

    /**
     * @description: 未经授权
     * @param {*} $message
     * @return {*}
     */
    public static function NoAuthorized($message = null)
    {
        if(is_null($message)) $message = "未经授权";
        self::getJsonResult(false, $message);
    }
}
