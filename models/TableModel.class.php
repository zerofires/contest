<?php
/*
 * @Author: your name
 * @Date: 2021-10-03 13:02:50
 * @LastEditTime: 2021-10-03 13:38:59
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \contest\models\TableModel.class.php
 */

 namespace models;

class TableModel
{
    public $code;
    public $msg;
    public $count;
    public $data;

    function __construct($code, $msg, $count, $data)
    {
        $this->code = $code;
        $this->msg = $msg;
        $this->count = $count;
        $this->data = $data;
    }
}
