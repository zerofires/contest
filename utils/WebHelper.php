<?php
class WebHelper
{
    public static function Logout($href)
    {
        session_start();
        session_unset();
        session_destroy();
        header("Location:" . $href);
    }
}
