function submit(t) {
  alert(t)
}

$(() => {

  $('#btn').on('click', function () {
    t = null;
  });

  $("form").on("submit", function (event) {
    event.preventDefault();

    let postdata = $('form').serialize();

    //加载中样式... 
    let loading = layer.msg("登录中", {
      icon: 16,
      shade: 0.3,
      time: 0
    });
    $.ajax({
      url: "action.php?a=login",
      method: 'post',
      data: postdata,
      success: function (data) {
        layer.close(loading);
        if (!data.success) {
          layer.msg(data.message)
        } else { //成功
          layer.closeAll();

          if (data.message && data.message.indexOf("已经参加了考试") > -1) {
            layer.open({
              type: 1,
              skin: 'layui-layer-rim', //加上边框
              area: ['420px', '240px'], //宽高
              content: `<div class="finalPop">
              <p>最终分数：${data.data}</p>
          </div>`,
            });
          } else {
            let loading = layer.msg("登录成功, 2 秒跳转页面！", {
              icon: 16,
              shade: 0.3,
              time: 0
            });
            setTimeout(function () {
              layer.close(loading);
              let url = data.data === '1' ? 'index.php' : 'back/index.php';
              window.location = url;
            }, 2000);
          }
        }
      },
      error: () => {
        layer.closeAll();
      }
    });

  });



});